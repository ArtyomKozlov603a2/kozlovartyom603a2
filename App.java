import Generator.CachedGenerator;
import Generator.Generator;
import Generator.SieveCachedGenerator;

/**
 * Примеры использования
 */
public class App {
    public static void main(String[] args) {
        Generator g1 = new Generator<Double>(x -> (Double) x[0] + (Double) x[1], 1., 1.);
        System.out.println(g1.get(20));
        System.out.println(g1.get(50));
        System.out.println(g1.get(5));
        Generator g7 = new Generator<Integer>(x -> (Integer) x[1], 1);
        System.out.println();
        Generator g = new Generator<Integer>(x -> (Integer)x[1] + (Integer)x[2] - (Integer)x[0], 1, 1, 2);
        System.out.println(g.get(100));

        Generator g2 = new CachedGenerator<Double>(x -> (Double) x[0] + (Double) x[1], 1., 1.);
        System.out.println(g2.get(20));
        System.out.println(g2.get(10));
        System.out.println(g2.get(50));
        Generator g3 = new CachedGenerator<Double>(x -> (Double) x[0] + (Double) x[1], 1., 1.);
        System.out.println(g3.get(20));
        System.out.println(g3.get(11));
        System.out.println(g3.get(51));
        Generator g10 = new CachedGenerator<Double>(x -> (Double) x[0] + (Double) x[1] - (Double) x[2], 1., 1., 2.);
        System.out.println(g10.get(1));
        System.out.println(g10.get(2));
        System.out.println(g10.get(3));
        System.out.println(g10.get(4));
        System.out.println(g10.get(5));
        System.out.println(g10.get(6));

        Generator g28 = new SieveCachedGenerator<Double>(0.2f, x -> (Double) x[0] + (Double) x[1], 1., 1.);
        System.out.println(g28.get(20));
        System.out.println(g28.get(10));
        System.out.println(g28.get(50) + "\n\n");
        Generator g4 = new SieveCachedGenerator<String>(0.2f, x -> x[0].toString() + x[1], "1", "2");
        System.out.println(g4.get(5));
        System.out.println("\n");
        Generator g5 = new SieveCachedGenerator<Double>(0.4f, x -> (Double) x[0] + (Double) x[1], 1., 1.);
        System.out.println(g5.get(20));
        System.out.println(g5.get(50));
        System.out.println(g5.get(5));
        Generator g6 = new SieveCachedGenerator<Double>(0.6f, x -> (Double) x[0] + (Double) x[1], 1., 1.);
        System.out.println(g6.get(20));
        System.out.println(g6.get(50));
        System.out.println(g6.get(5));
        Generator g6500 = new SieveCachedGenerator<Double>(0.3f, x -> (Double) x[0] + (Double) x[1] + (Double) x[2], 1., 1., 2.);
        System.out.println(g6500.get(20));
        System.out.println(g6500.get(50));
        System.out.println(g6500.get(5));
        System.out.println(g6500.get(7));
        System.out.println(g6500.get(8));
        System.out.println(g6500.get(9));
        System.out.println(g6500.get(10));
        System.out.println(g6500.get(11));
        System.out.println(g6500.get(12));
        System.out.println(g6500.get(13));
        System.out.println(g6500.get(14));
        System.out.println(g6500.get(15));
        System.out.println(g6500.get(16));
        System.out.println(g6500.get(17));
        Generator g6501 = new SieveCachedGenerator<Double>(0.8f, x -> (Double) x[0] + (Double) x[1] + (Double) x[2] + (Double) x[3], 1., 1., 2., 5.);
        System.out.println("\n\n" + g6501.get(1));
        System.out.println(g6501.get(2));
        System.out.println(g6501.get(4));
        System.out.println(g6501.get(3));
        System.out.println(g6501.get(6));
        System.out.println(g6501.get(5));
        System.out.println(g6501.get(10));
        System.out.println(g6501.get(15));
    }
}
