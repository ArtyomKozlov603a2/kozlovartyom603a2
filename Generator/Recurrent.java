package Generator;

/**
 * Рекуррентная формула, позволяющая получить
 * очередной член последовательности
 * на основе нескольких предыдущих
 */
@FunctionalInterface
public interface Recurrent<T> {

    /**
     *
     * @param previous несколько предыдущих членов ряда
     * @return следующий член ряда
     */
    T next(Object... previous);
}
