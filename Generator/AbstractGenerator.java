package Generator;

import java.util.Arrays;

/**
 * Генератор, позволяющие получать
 * произвольный член произвольной последовательности,
 * заданной рекуррентной формулой
 */
public abstract class AbstractGenerator<T> {
    T[] startValues;
    int startLength;
    Recurrent<T> rule;

    /**
     * Создает генератор с заданной рекуррентной формулой и начальными значениями
     *
     * @param rule функция, вычисляющая очередной член последовательности
     * @param starts начальные члены последовательности
     * @throws IllegalArgumentException если задана недопустимая формула
     */
    @SafeVarargs
    AbstractGenerator(Recurrent<T> rule, T... starts) throws IllegalArgumentException {

                this.startValues = starts;
                this.startLength = starts.length;
                this.rule = rule;
                try {
                    rule.next(startValues);
                } catch (ArrayIndexOutOfBoundsException e) {
                    throw new IllegalArgumentException("Задана неверная рекуррентная формула. Недостаточно начальных значений.");
                }
        }


    /**
     * Возвращает произвольный член последовательности
     *
     * @param n номер члена последовательности, n > 0
     * @return заданный член последовательности
     * @throws IndexOutOfBoundsException если n < 1
     */
    public abstract T get(int n) throws IndexOutOfBoundsException;
}
