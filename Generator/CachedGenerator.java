package Generator;

import java.util.Arrays;

public class CachedGenerator<T> extends Generator<T> {
    private T[] cash;
    /**
     * Создает генератор с полным заполнением
     *
     * @param rule функция, вычисляющая очередной член последовательности
     * @param starts начальные члены последовательности
     * @throws IllegalArgumentException если задана недопустимая формула
     */
    @SafeVarargs
    public CachedGenerator(Recurrent<T> rule, T... starts) throws IllegalArgumentException {
        super(rule, starts);
        cash = Arrays.copyOf(startValues, startLength);
    }

    @Override
    public T get(int n) throws IndexOutOfBoundsException {
        try {
            if (n < 1) {
                throw new IndexOutOfBoundsException("Порядковый номер числа меньше еденицы. " +
                        "\nCachedGenerator -> get -> Число " + n + " посланое в метод меньше 1.");
            }
        } finally {}
        if (cash.length < n) {
            int k = cash.length;
            cash = Arrays.copyOf(cash, n);
            T[] tcash = Arrays.copyOf(startValues, startLength);
            for (int i = k; i < n; ++i) {
                for (int j = 0; j < startLength; ++j)
                    tcash[j] = cash[k - (startLength - j)];
                cash[i] = rule.next(tcash);
                ++k;
            }
            return cash[cash.length - 1];
        } else {
            return cash[n - 1];
        }
    }
}
