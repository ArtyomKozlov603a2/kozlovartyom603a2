package Generator;

import java.lang.reflect.Array;
import java.util.Arrays;

public class Generator<T> extends AbstractGenerator<T> {
    /**
     * Создает генератор
     *
     * @param rule   функция, вычисляющая очередной член последовательности
     * @param starts начальные члены последовательности
     * @throws IllegalArgumentException если задана недопустимая формула
     */
    @SafeVarargs
    public Generator(Recurrent<T> rule, T... starts) throws IllegalArgumentException {
        super(rule, starts);
    }

    @Override
    public T get(int n) throws IndexOutOfBoundsException {
        try {
            if (n < 1) {
                throw new IndexOutOfBoundsException("Порядковый номер числа меньше еденицы. " +
                        "\nGenerator -> get -> Число " + n + " посланое в метод меньше 1.");
            }
        } finally {}
        T k;
        T[] p0 = Arrays.copyOf(startValues, (startLength - 1));
        T[] p1 = Arrays.copyOf(startValues, startLength);
        for (int i = 0; i < startLength; ++i)
            p1[i] = startValues[i];
        for (int i = 0; i < (n - startLength); ++i) {
            for (int j = 0; j < startLength - 1; ++j)
                p0[j] = startValues[j + 1];
            startValues[startLength - 1] = rule.next(startValues);
            for (int j = 0; j < startLength - 1; ++j)
                startValues[j] = p0[j];
        }
        k = startValues[startLength - 1];
        for (int i = 0; i < startLength; ++i)
            startValues[i] = p1[i];
        return k;
    }
}
