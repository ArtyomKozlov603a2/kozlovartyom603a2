package Generator;

import java.util.Arrays;

public class SieveCachedGenerator<T> extends CachedGenerator<T>{
    private float proc;
    private T[] cash;
    /**
     * Создает генератор с частичным заполнением
     *
     * @param filling плотность заполнения кэша в процентах
     * @param rule функция, вычисляющая очередной член последовательности
     * @param starts начальные члены последовательности
     * @throws IllegalArgumentException если задано недопустимое заполнение или недопустимая формула
     */
    @SafeVarargs
    public SieveCachedGenerator(float filling, Recurrent<T> rule, T... starts) throws IllegalArgumentException {
        this(rule, starts);
        cash = Arrays.copyOf(startValues, startLength);
        proc = filling;
    }

    /**
     * Создает генератор с полным заполнением
     *
     * @param rule функция, вычисляющая очередной член последовательности
     * @param starts начальные члены последовательности
     * @throws IllegalArgumentException если задана недопустимая формула
     */
    @SafeVarargs
    public SieveCachedGenerator(Recurrent<T> rule, T... starts) throws IllegalArgumentException {
        super(rule, starts);
        cash = Arrays.copyOf(startValues, startLength);
        proc = 1;
    }

    @Override
    public T get(int n) throws IndexOutOfBoundsException {
        try {
            if (n < 1) {
                throw new IndexOutOfBoundsException("Порядковый номер числа меньше еденицы. " +
                        "\nSieveCachedGenerator -> get -> Число " + n + " посланое в метод меньше 1.");
            }
        } finally {}
        T cash3;
        int cyclicality = (int) (startLength / proc);
        int req_e = (((n - 1) / cyclicality) * startLength) + startLength;
        if (req_e > cash.length) {
            while (req_e > cash.length) {
                T[] tcash = Arrays.copyOf(startValues, startLength);
                T[] tycash = Arrays.copyOf(tcash, startLength);
                for (int i = 0; i < startLength; ++i)
                    tcash[i] = cash[cash.length - (startLength - i)];
                for (int i = 0; i < cyclicality; ++i) {
                    for (int j = 0; j < startLength; ++j)
                        tycash[j] = tcash[j];
                    tcash[startLength - 1] = rule.next(tcash);
                    for (int j = 0; j < startLength - 1; ++j)
                        tcash[j] = tycash[j + 1];
                }
                cash = Arrays.copyOf(cash, (cash.length + startLength));
                for (int i = cash.length - startLength; i < cash.length; ++i)
                    cash[i] = tcash[(i + startLength) - cash.length];
            }
        }
        int k = (n - 1) % cyclicality;
        if (k < startLength) {
            cash3 = cash[req_e - (startLength - k)];
        } else {
            T[] tcash = Arrays.copyOf(startValues, startLength);
            T[] tycash = Arrays.copyOf(startValues, startLength);
            for (int i = 0; i < startLength; ++i)
                tcash[i] = cash[req_e - startLength + i];
            for (int i = 0; i < ((k + 1) - startLength); ++i) {
                for (int j = 0; j < startLength; ++j)
                    tycash[j] = tcash[j];
                tcash[startLength - 1] = rule.next(tcash);
                for (int j = 0; j < startLength - 1; ++j)
                    tcash[j] = tycash[j + 1];
            }
            cash3 = tcash[startLength - 1];
        }
        return cash3;
    }

}
